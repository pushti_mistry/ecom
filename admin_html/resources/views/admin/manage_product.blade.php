@extends('admin/layout')
@section('page_title',' Manage Product')
@section('product_select','active')
@section('container')
    @if($id>0)
        {{$image_required=""}}
    @else
        {{$image_required="required"}}
    @endif
    <h1 class="mb10">Manage Product </h1>
    <a href="{{url('admin/product')}}">
        <button type="button" class="btn btn-success">
            Back
        </button>
    </a>
    <div class="row m-t-30">
        <div class=col-md-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route(product.manage_product_process)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="Product Name" class="control-label mb-1">
                                        Product Name
                                    </label>
                                    <input id="product_name" value="{{$product_name}}" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('product_name')
                                        <div class="alert alert-danger" role="alert">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="name_slug" class="control-label mb-1">
                                        Product Slug
                                    </label>
                                    <input id="product_slug" value="{{$product_slug}}" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('product_slug')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="category_id"  class="control-label mb-1">
                                        Category
                                    </label>
                                    <select id="category_id" name="category_id" class="form-control" required>
                                        <option value="  ">Select Categories</option>
                                        @foreach($category as $list)
                                            @if($category_id==$list->id)
                                                <option value="{{$list->id}}" selected>{{$list->category_name}}</option>
                                            @else
                                                 <option value="{{$list->id}}">{{$list->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="image" class="control-label mb-1">
                                        Image
                                    </label>
                                    <input id="file"  type="file" name="file" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('image')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="price" class="control-label mb-1">
                                        Price
                                    </label>
                                    <input id="price" value="{{$price}}" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('price')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label mb-1">
                                        Description
                                    </label>
                                    <textarea id="description"  type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                        {{$description}}
                                    </textarea>
                                    @error('description')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>



                                <div>
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">Submit</button>
                                </div>
                                <input type="hidden" name="id" value="{{$id}}">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
