<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Http\Support\Facade\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result['data']=Product::all();
        return view('admin/product',$result);
    }

   public function manage_product(Request $request,$id=''){
        if($id>0){
            $arr=Product::where(['id'=>$id])->get();
            $result['name']=$arr['0']->product_name;
            $result['category']=$arr['0']->product_category;
            $result['image']=$arr['0']->product_image;
            $result['price']=$arr['0']->product_price;
            $result['description']=$arr['0']->product_description;
            $result['status']=$arr['0']->product_status;
            $result['product_slug']=$arr['0']->product_slug;
            $result['id']=$arr['0']->id;
        }
        else{
            $result['name']='';
            $result['category']='';
            $result['image']='';
            $result['price']=0;
            $result['description']='';
            $result['status']=0;
            $result['product_slug']='';
            $result['id']=0;
        }
        $result['category']=DB::table('categories')->where(['status'=>1])->get();
        return view('admin/manage_product',$result);
   }
   public function manage_product_process(Request $request){
        if($request->post('id')>0){
            $image_validation="mimes:jpeg,jpg,png";
        }
        else{
            $image_validation="required|mimes:jpeg,jpg,png";
        }
       $request->validate([
           'product_name'=>'required',
           'product_image'=>$image_validation,
           'product_slug'=>'required|unique:products,slug,'.$request->post('id'),
           ]);
       if($request->post('id')>0){
           $model=Product::find($request->post['id']);
           $msg="product updated";
       }
       else{
           $model= new Product();
           $msg="product inserted";
       }
       if($request->hasfile('image')){
           $image=$request->file('image');
           $ext=$image->extension();
           $image_name=time().'.'.$ext;
           $image->storeAs('/public/media',$image_name);
           $model->image=$image_name;
       }
       $model->product_name=$request->post('name');
       $model->product_slug=$request->post('product_slug');
       $model->product_category=$request->post('category');
       $model->product_image=$request->post('image');
       $model->product_description=$request->post('description');
       $model->product_category=$request->post('category');
       $model->product_category=$request->post('category');
       $model->product_category=$request->post('category');

       $model->status=1;
       $model->save();
       $request->session()->flash('message',$msg);
       return redirect('admin/product');
   }
   public function delete(Request $request,$id){
        $model=Product::find($id);
        $model->delete();
       $request->session()->flash('message','product deleted');
       return redirect('admin/product');
   }
   public function status(Request $request,$status,$id){
       $model=Product::find($id);
       $model->status=$status;
       $model->save();
       $request->session()->flash('message','product status updated');
       return redirect('admin/product');

   }
}
