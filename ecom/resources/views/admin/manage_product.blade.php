@extends('admin/layout')
@section('page_title',' Manage Product')
@section('product_select','active')
@section('container')
    @if($id>0)
        {{$image_required=""}}
    @else
        {{$image_required="required"}}
    @endif
    <h1 class="mb10">Manage Product </h1>
    <a href="{{url('admin/product')}}">
        <button type="button" class="btn btn-success">
            Back
        </button>
    </a>
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('product.manage_product_process')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name" class="control-label mb-1">
                                        Product Name
                                    </label>
                                    <input id="name"  value="{{$name}}" type="text" name="name" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('name')
                                        <div class="alert alert-danger" role="alert">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="category"  class="control-label mb-1">
                                        Category
                                    </label>
                                    <select id="id" name="category" class="form-control" required>
                                        <option value="  ">Select Categories</option>
                                        @foreach($category as $list)
                                            @if($id==$list->id)
                                                <option value="{{$list->id}}" selected>{{$list->category_name}}</option>
                                            @else
                                                <option value="{{$list->id}}">{{$list->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="image" class="control-label mb-1">
                                        Image
                                    </label>
                                    <input id="image"  type="file" name="image" class="form-control" aria-required="true" aria-invalid="false" {{$image_required}}>
                                    @error('image')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="price" class="control-label mb-1">
                                        Price
                                    </label>
                                    <input id="price" value="{{$price}}" name="price" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('price')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label mb-1">
                                        Description
                                    </label>
                                    <textarea id="description"  name="description" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                        {{$description}}
                                    </textarea>
                                    @error('description')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label for="slug" class="control-label mb-1">
                                        Product Slug
                                    </label>
                                    <input id="slug" value="{{$slug}}" name="slug" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                    @error('slug')
                                    <div class="alert alert-danger" role="alert">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>



                                <div>
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">Submit</button>
                                </div>
                                <input type="hidden" name="id" value="{{$id}}">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
