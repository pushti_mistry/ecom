<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function index(){
        $result['home_categories']=DB::table('categories')->where(['status'=>1])->get();
       /* foreach($result['home_categories'] as $list) {
            $result['home_categories'][$list->id] =
                DB::table('products')
                    ->where(['status' => 1])
                    ->where(['category' => $list->id])
                    ->get();
        }
*/
        /*echo "<pre>";
        print_r( $result);
        echo "</pre>";
        die();*/

        return view('front.index',$result);
    }
}
