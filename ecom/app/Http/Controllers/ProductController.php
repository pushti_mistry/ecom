<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result['data']=Product::all();
        return view('admin/product',$result);
    }

   public function manage_product(Request $request,$id=''){
        if($id>0){
            $arr=Product::where(['id'=>$id])->get();
            $result['name']=$arr['0']->name;
            $result['category']=$arr['0']->category;
            $result['image']=$arr['0']->image;
            $result['price']=$arr['0']->price;
            $result['description']=$arr['0']->description;
            $result['status']=$arr['0']->status;
            $result['slug']=$arr['0']->slug;
            $result['id']=$arr['0']->id;
        }
        else{
            $result['name']='';
            $result['category']='';
            $result['image']='';
            $result['price']=0;
            $result['description']='';
            $result['status']=0;
            $result['slug']='';
            $result['id']=0;
        }
        $result['category']=DB::table('categories')->where(['status'=>1])->get();
        return view('admin/manage_product',$result);
   }
   public function manage_product_process(Request $request){
        if($request->post('id')>0){
            $image_validation="mimes:jpeg,jpg,png";
        }
        else{
            $image_validation="required|mimes:jpeg,jpg,png";
        }
       $request->validate([
           'name'=>'required',
           'image'=>$image_validation,
           'slug'=>'required|unique:products,slug,'.$request->post('id'),
           ]);
       if($request->post('id')>0){
           $model=Product::find($request->post('id'));
           $msg="product updated";
       }
       else{
           $model= new Product();
           $msg="product inserted";
       }
       if($request->hasfile('image')){
           echo 'h';
           $image=$request->file('image');
           $ext=$image->extension();
           $image_name=time().'.'.$ext;
           $image->storeAs('/public/media',$image_name);
           $model->image=$image_name;
       }
//       dd($model->toArray());
       $model->name=$request->post('name');
       $model->category=$request->post('category');
       //$model->image=$request->post('image');
       $model->price=$request->post('price');
       $model->description=$request->post('description');
       $model->slug=$request->post('slug');

       $model->status=1;
       $model->save();
       $request->session()->flash('message',$msg);
       return redirect('admin/product');
   }
   public function delete(Request $request,$id){
//        echo 'here';die;
        $model=Product::find($id);
        $model->delete();
       $request->session()->flash('message','product deleted');
       return redirect('admin/product');
   }
   public function status(Request $request,$status,$id){
       $model=Product::find($id);
       $model->status=$status;
       $model->save();
       $request->session()->flash('message','product status updated');
       return redirect('admin/product');

   }
}
